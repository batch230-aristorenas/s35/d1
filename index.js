const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Connecting to MongoDB locally
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Syntax:
	// mongoose.connect("<MongoDB connection string>", {urlNewUrlParser: true})
mongoose.connect("mongodb+srv://admin:admin@batch230.d2wmu0h.mongodb.net/S35?retryWrites=true&w=majority", 
	{ 
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
)

// Connection to database
// Allows to handle errors when the initial connection is established
// Works with the on and once Mongoose methods
let db = mongoose.connection

// If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));

app.use(express.json());

// [Section] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database
// Server > Schema (blueprint) > Database > Collection

// The variable/object "Task"can now used to run commands for interacting with our database
// "Task" is capitalized following the MVC approach for naming conventions
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (request, response) => {

	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name: request.body.name}, (err, result) => {
		// If there was found and the document's name matches the information via the client/Postman
		if(result!=null && result.name == request.body.name) {
			return response.send("Duplicate task found");
		}
		else{

			let newTask = new Task({
				name: request.body.name
			})

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
			newTask.save((saveErr, savedTask)=>{
				// if an error is saved in saveErr parameter
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New task created");
				}
			})

		}
		
	})
})	

app.get("/tasks", (request, response) => {

	Task.find({}, (err, result) => {

		if(err){
			return console.log(err);
		}
		else{
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return response.status(200).json({
				data : result
			})
		}

	})

})



app.listen(port, ()=>console.log(`Server running at port ${port}`));

